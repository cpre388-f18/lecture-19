package edu.iastate.cpre388.lecture19_pendingintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class NotificationActivity extends AppCompatActivity {
    public static String EXTRA_STR = "edu.iastat.cpre388.notification_data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        TextView tv = findViewById(R.id.notificationTextView);
        Intent i = getIntent();
        tv.setText(i.getStringExtra(EXTRA_STR));
    }
}
