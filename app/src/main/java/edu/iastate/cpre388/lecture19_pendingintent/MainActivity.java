package edu.iastate.cpre388.lecture19_pendingintent;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String CHANNEL_ID = "Main Channel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View v) {
        // Create an explicit intent
        Intent action = new Intent(this, NotificationActivity.class);
        action.putExtra(NotificationActivity.EXTRA_STR, "Test string");
        action.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Package the intent into a pending intent
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, action, 0);

        // Build a notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("My notification")
                .setContentText("My notification text")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        // Locate the notification manager and send the notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // Todo: keep the id below to canceling or changing later
        notificationManager.notify(0, builder.build());
    }
}
